# 1. Documentation

J'ai appris à utiliser Git afin de me préparer à documenter correctement mon travail, mais comment ai-je mené à bien cette mission ? 

Dans cette section, je vous expliquerai mon processus de prise en main de GitLab, de la mise en forme par markdown à la connexion SSH.

## Telechargement de Git et première connexion 

Etant possesseur d'un PC windows, j'ai du [télécharger](https://git-scm.com/)et installer git sur mon pc afin d'avoir accès à _**Bash**_, l'invité de commande que j'utiliserai pour relier mon pc à GitLab. 
Premièrement, j'ai connecté mon compte git sur Bash via les commandes suivantes:
```
git config --global user.name "lionel.compereleroy"
git config --global user.email "lionel.compere@gmail.com"
```

## Liaison SSH avec mon ordinateur

Afin de permettre une modification de mon GitLab localement via mon ordinateur, j'ai relié mon respository GitLab aux fichiers de mon ordinateur via une clef SSH. Cela m'autorisera via l'invité de commandes Bash à envoyer des instruction directement sur GitLab. 

### Génération d'une clef SSH
Premièrement, j'ai crée une clef SSH à partir de mon terminal Bash, avec la commande suivante:

```
ssh-keygen -t ed25519 -C "lionel.compereleroy"
```
Bash demande ensuite la création d'un mot de passe, afin de sécuriser la connexion. 
### Ajout de la clef ssh à GitLab
Une fois la clef générée, il est nécéssaire de l'encoder dans GitLab afin de créer un pont avec le terminal. Pour cela, j'ai copié le contenu du fichier .pub (clef publique) généré par Bash et l'ai collé dans _"clé SSH"_, situé dans les paramètres utilisateurs de GitLab. Le nom de la clef s'encode automatiquement avec le nom intégré dans la commande keygen éffectuée précédemment. Une date d'expiration doit également être fixée.

![](/docs/images/addssh.jpg) 

Une fois cette manipulation effectuée, on peut cloner le repository GitLab localement sur l'ordinateur, ce qui permet de travailler hors ligne, en simultané avec quelqu'un d'autre, ou encore d'utiliser un éditeur de texte à des fins de personnalisation du site.

### Utilisation de Bash dans le dossier choisi pour le clonage du repository
Pour utiliser Bash dans le dossier de mon choix qui sera plus tard le dossier de destination du clone de mon repository Git, j'ai ouvert Bash en effectuant un clic droit sur le dossier _"lionel.compereleroy"_ que j'ai crée dans le dossier utilisateur de mon pc.

![](../images/open-bash.png) 

### Clonage du repository sur mon PC

Pour cloner le projet GitLab localement, premièrement j'ai copié l'URL ssh trouvé sur la page d'accueil de celui ci comme ci dessous.

![](../images/CLONE.jpeg)

Ensuite, j'ai effectué cette commande dans mon dossier de destination via Bash pour générer le clone:
```
git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy.git
```
## Interactions avec Git depuis l'invité de commande
Certaines commandes sont cruciales dans l'utilisation de Git, et doivent TOUJOURS être effectuées dans le fichier contenant le clone du repository via bash (comme expliqué plus haut).
### Ajouter des fichiers au repository
La commande suivante sert à ajouter les fichiers
```
git add file_1 file_2
```
Il faut ensuite confirmer cette modification, et éventuellement la commenter (d'où le _"-m"_) pour garder une trace de celle-ci comme suit
```
git commit -m "commentaire"
```
Pour que la modification soit effectuée sur le site de destination, il faut ensuite effectuer cette commande:

```
git push
```
## Commandes markdown utiles
Sur GitLab, il est possible de directement modifier son site via l'IDE. Voici les commandes que j'ai le plus utilisé :
* **Mise en forme du texte**
```
**text**        #mettre du texte en gras
#text           #titre principal
##text          #sous-titre (un "#" en plus reduit le niveau du titre, et permet donc de creer des sous-parties) 
_text_          #italique
# ```
mycode
# ```           # (sans les "#" avant les " ``` ") inclure du code à la page

```
* **Intégration d'éléments médias à la page**
```
[](www.url.com)                                             #intégration d'un lien cliquable vers un site (remplir le "[]" permet d'indiquer ce qui sera ecrit sur le lien cliquable)
![](../images/image.jpeg)                                   #intégration d'une image provenant de mon dossier image
<img src="../images/image.jpeg" >                           #intégration d'une image provenant de mon dossier image /!\ update: INCOMPATIBLE AVEC TRANSFERT SUR LE SITE
<img src="../images/image.jpeg" width="800" height="250">   #intégration d'une image redimensionnée 
<iframe width="560" height="315" src="https://youtu.be/nyJ_kJ0ed2g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>                                                    
                                                            #intégration d'un cadre vidéo dans la page

```

## Manipulation d'images 
Pour documenter correctement mon travail, il est important de savoir manipuler les images, c'est à dire les redimensionner, et réduire leur taille à environ 100 ko maximum.

### Réduction de la taille d'images 

#### Via gimp
[Gimp](https://www.gimp.org/downloads/) est un éditeur d'image qui permet entre autres de réduire la taille des images que l'on y importe. Pour ce faire, il suffit de se rendre dans le menu image, de séléctionner _"Echelle et taille de l'image"_ et ensuite de réduire la définition avant de l'exporter

![](../images/gimp-SIZE.jpeg)

#### Via l'application [_PicTools_](https://play.google.com/store/apps/details?id=omkar.tenkale.pictoolsandroid&hl=fr&gl=US)
Pour les photos prises à l'aide de mon téléphone, il était plus simple pour moi de réduire directement leur taille via mon téléphone avant de me les partager par mail. L'intérêt de cette application est que l'on peut choisir la taille ou définition cible de la photo manipulée, et que l'on peut réduire jusqu'à 25 images simultanément. 

![](../images/image-reduce.jpeg)

Il suffit ensuite de les importer sur GitLab ou elles peuvent être intégrées au projet.

### Redimensionnement d'images 
#### Via gimp
Par la même démarche que pour la réduction de taille, mais en modifiant cette fois la largeur et la hauteur en pixels .
#### Via markdown
Cf ci-dessus, dans les commandes d'intégration de médias (.... width="xxx" height="xxx" ).

### rogner des images
Le plus simple pour rogner les images selon moi est d'utiliser l'option rogner dans les applications natives, que ce soit sur mon téléphone ou mon PC.

