# 3. Impression 3D
L'impression 3D est un outil puissant permettant de fabriquer presque n'importe quel objet modélisé à l'aide de logiciels de conceptions 3D. On l'utilise pour créer des pièces personnalisées qu'on ne pourrait pas trouver dans le commerce, ou pour prototyper facilement les composants d'un projet. Cet outil bien qu'utile n'est pas parfait, voici les avantages et inconvénients de l'impression 3D:

* **Avantages de l'impression 3D**

  -Créer un objet en 3D modélisé par ordinateur

  -Grande précision  

  -Prix abordable des matériaux et de l'appareil

  -Simple d'utilisation

* **Inconvénients de l'impression 3D**

  -Temps d'impression tres élevé

  -Peu de matériaux compatibles (doit pouvoir fondre puis se solidifier instantanément)

  -Taille d'impression limitée à quelque dizaines de centimètres 

En résumé, l'impression 3D est parfaite pour le prototypage, mais ne sera pas le premier choix quand une autre méthode de conception est possible, telle que la découpe laser. De plus, sa lenteur empêche l'utilisation à grande échelle, sauf dans de rare cas tels que la ferme à imprimantes 3D de Prusa. 

<iframe width="682" height="384" src="https://www.youtube.com/embed/qqQzTvvrXo8" frameborder="0" allow="autoplay; encrypted-media; " allowfullscreen></iframe>



## Impression d'un Flexlink

/!\ ce module n'est pas encore complet, car je dois encore rajouter le modèle d'un collègue afin de créer mon kit, ainsi qu'imprimer ce kit sur une des Prusa du fablab. Je n'ai pas encore eu l'occasion de passer à l'impression en raison de ma mission de mémoire aux iles Canaries et de la fermeture du fablab durant le congé de Paques. Je completerai donc cette mission dès la rentrée.

Lors de la lecon de conception 3D, j'ai crée [mon modèle](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/exo-FLEX.scad/) de Flexlink. Le but du travail pratique d'impression était donc d'imprimer son FlexLink. Nous disposons au fablab d'imprimantes Prusa dont les propriétés sont les suivantes:
  -Modèle: Prusa I3MK3S
  
  -Taille du plateau d’impression : 22,5cm x 22,5cm

  -Hauteur d’impression maximale : 25 cm

  -Matériau utilisé : PLA

  -Diamètre de buse: 0,4mm

J'ai également pour projet de fusionner mon FlexLink avec celui d'un autre étudiant afin d'obtenir un ki de fabrication de compliant mecanism.



 Pour cela, il faut suivre les étapes suivantes:

### Préparation du fichier d'impression via le [slicer prusa](https://help.prusa3d.com/en/downloads/)

Un slicer est un logiciel qui peut à partir d'un fichier 3D créer ce qu'on appelle un _"G-code"_, qui consiste en une série d'instructions sur les opérations à effectuer par l'imprimante. Le slicer comme son nom l'indique découpe l'objet en plusieurs couches qui correspondent chacune à des couches de PLA qui seront déposées par l'imprimante.

Pour avoir un fichier exploitable par le slicer, j'ai exporté mon modèle openscad au format .STL. Ensuite à partir du slicer de Prusa, j'ai séléctionné "importer un fichier STL" et mon modèle apparait sur le plateau  
![](../images/3dprint-model.JPG)

La totalité des informations concernant les différents paramètre peut être trouvée [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md)

#### Paramétrage d'impression

Différents paramètre doivent être choisis avant de lancer l'impression, ils dépendent des propriétés physiques attendues de l'objet à imprimer, mais aussi des caractéristiques de l'imprimante.

* **Hauteur de couche**

La hauteur de couche est la hauteur de chaque couche de plastique qui sera déposée par la buse. Il faut pour une impression optimale définir:

hauteur de couche =  hauteur de buse * 1/2

La [hauteur de buse standard](https://help.prusa3d.com/fr/article/differents-types-de-buse_2193/) de la Prusa est de 0,4 mm, et j'ai donc choisi une hauteur de couche de 0,2mm.

![](../images/hauteur-buse.JPG)



* **Parois et coques**

La paroi ou coque est la partie externe (en orange), qui contrairement à l'intérieur de la pièce (en mauve) est plein, afin d'avoir un objet d'apparence lisse comme on peut le voir ci-dessous.

![](../images/coque.JPG)

Un objet comme un flexlink qui est fait pour subir des contraintes physique doit disposer d'une coque assez épaisse. Un minimum de 3 couches est requis pour les parois verticales, raison pour laquelle j'en avais mis 4 afin d'être sur que le résultat soit assez robuste. Cependant je me suis rendu compte qu'il valait peut être mieux en laisser seulement 3 pour qu'il reste une partie non-pleine dans la structure de manière à la rendre plus flexible.

![](../images/paroi-setup.JPG)


* **Remplissage**

Pour un objet qui subit des contraintes, un remplissage en nid d'abeilles est optimal, car il réparti les contraintes efficacement. Je n'arraivais pas à visualiser le remplissage correctement sur le slicer, mais voilà un exemple de remplissage en nid d'abeille.

![](../images/honeycomb.JPG)

Pour la densité du remplissage, pour un objet fonctionnel destiné à subir des contraintes physiques telles que le flexlink, il faut viser au dessus des 20%, raison pour laquelle j'ai choisi 25% de remplissage

Voici comment encoder ces données dans le slicer:

![](../images/fill.JPG)


* **Jupe et bordure**

La jupe sert à vérifier que l'objet ne dépasse pas des limites de l'imprimante, elle entoure le périmètre d'impression comme on peut le voir ici.

![](../images/jupe-slicer.JPG)

J'ai laissé les paramètres par défaut de la jupe, car ceux-ci n'impactent en rien les propriétés de l'objet, c'est juste un indicateur visuel

![](../images/jupe-setup.JPG)

La bordure sert à stabiliser un objet de grande taille en servant de socle pour le maintenir droit par la base. Normalement elle n'est pas requise pour les flexlinks car ces objets sont très bas, mais j'ai entendu plusieurs expériences d'objets qui se décalaient en pleine impression, c'est pourquoi j'ai décidé de laisser tout de même une petite bordure de 1mm de manière à être sur de la stabilité de mon FlexLink durant impression.

![](../images/bordure-setup.JPG)
![](../images/bordure.JPG)



* **Supports**

Les supports sont utiles pour les objets ayant des angles en aplomb au dessus du vide, afin de maintenir les parties n'ayant pas de couche inférieure directe pour les supporter.

C'est pour ca qu'il est important d'orienter correctement sa pièce sur le plateau afin d'avoir si possible le moins de parties possible en applomb au dessus du vide.

Pour le Flexlink, il est orienté de manière à pouvoir être imprimé sans supports, voici un exemple d'une  **mauvaise orientation** du FlexLink qui demanderait des supports (on peut visualiser les zones de surplomb en bleu):

![](../images/surplomb.JPG)


#### Paramétrage du filament
Le filament exploité par la Prusa est d'un diamètre de 1.75 mm, et les températures d'extrusion exploitées sont de :

* 215 degrés pour l'extrudeur, température destinée à arriver au point de fusion du PLA 

* 60 degrés pour le plateau, température permettant de décoller la pièce après impression.

![](../images/filament.JPG)

#### Génération et exportation du G-code

Le G-code est le format qui permet à l'imprimante de recevoir les instructions à effectuer physiquement (ordre de déplacement de la buse, températures à atteindre,...). En lancant ce code sur la Prusa, l'impression peut se dérouler en accord avec tous les paramètres séléctionnés précédemment.

* **Export du G-code**
Voilà la marche à suivre pour obtenir le fichier _"G-code"_ après définition des paramètres.

![](../images/export-gcode.JPG)

Le G-code peut ensuite être sauvgardé sur une carte SD qui doit ensuite être insérée dans la prussa pour lancer l'impression.

### Impressions

L'impression peut ensuite être lancée. 

Cependant, avant d'imprimer le modèle final, il est bien de faire d'une part une calibration, et d'autre part un torture-test afin de s'assurer que l'impression se déroulera au mieux.

####Torture test
Sert à obtenir un modèle 3D contenant de nombreux étalons testant les limites de l'imprimante et contenant des tests de:

* **Limite d'impression en aplomb**: teste l'angle limite d'impression au dessus du vide

* **De supports**: teste la génération de supports

* **D'échelle**: permet de voir si les dimensions théoriques sont respectées lors de l'impression

* **De _Bridging_** : permet de voir si l'impression reliant 2 points élevés au dessus du vide fonctionne, et dans quelles mesures 

Le test pertinent à analyser dans le cas des FlexLink est le test d'échelle qui permet de savoir quel ajustement faire sur la taille pour obtenir une taille réelle compatible avec les LEGO.
 

#### Kit FlexLink

N'ayant pas d'idées de mécanisme, je me suis dit que j'allais imprimer diverses pièces FlexLinks afin d'inventer un mécanisme directement avec les pièces physiques en main.

Une fois toutes les manipulations de slicing effectuées, l'impression finale peut être lancée.
En plus de mes 2 pièces, je me suis servi des pièces designées par: [Floriane Weyer](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/Cross_axis_Floriane_Weyer_.scad/#), [Doriane GALBEZ 1](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/curvedscad.scad/# ), [Doriane GALBEZ 2](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/satellite.scad/#) et [Kristine VALAT](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/unfixed.scad/#).
J'ai uniformisé les dimensions des trous afin d'avoir le meme format pour toutes les pièces. Ayant vu dans la plupart des documentations qu'il valait mieux encoder 2,5 mm de rayon plutôt que 2,4mm afin de rendre la pièce compatible avec les LEGOs, j'ai choisi un rayon de trou de 2,5 mm.

##### 1. Impression des Flexlinks

* Voilà ma plaque d'impression au complet sur le slicer de la Prusa :

![](../images/flexkit-prusa.JPG)

* Mes impressions ne se sont pas toutes passées comme prévu... :

**Première tentative** : l'imprimante "ZOZIE" était mal calibrée, ce qui a induit une erreur tot dans l'impression des pièces: de fils s'étalant de plus en plus comme on peut le voir ici m'ont contraint à stopper l'impression. L'épaisseur a donc été réduite à 1-2 mm, ce qui reste peut être exploitable dans l'utilisation des FlexLink comme flexture mecanism.

![](../images/fail-flexkit.jpeg)

**Deuxième tentative** : j'ai sur conseil d'une responsable des impressions 3D au Fablab changé de motif de remplissage: rectiligne au lieu de nid d'abeille, car apparement plus optimisé pour les pièces de petite taille. L'imprimante "LILI" a fonctionné correctement pour l'impression complète de 2 de mes 3 pièces, la troisième s'étant décollée durant son impression.

![](../images/fail-flexkit-2.jpeg)

##### 2. Conception des pièces mâles "LEGO"

J'ai décidé d'imprimer des pièces LEGO plates afin d'aller avec mes FlexLinks. J'ai trouvé un [code OpenScad](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/LEGO.scad/#https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/LEGO.scad/#) **INCROYABLE** qui permet de paramètrer une pièce LEGO de la manière souhaitée en choisissant le nombre de trous, la largeur, la présence de trous horizontaux,... il est en somme possible de recréer n'importe quelle pièce de LEGO à partir de ce fichier. 

* **Etalonnage** de mon kit 

Il me faut des pièces LEGO qui se fixent de manière assez forte aux FlexLinks. Afin d'être sur d'imprimer des pièces adaptées, j'ai fait un test en imprimant la même pièce (plaque 2x1) dans 4 échelles différentes: 95% - 100% - 105% - 110%

![](../images/flex-calibration.JPG)

J'ai essayé d'assembler chacun de ces 4 tests avec mes FlexLinks, et le 95% est celui qui a le mieux marché, raison pour laquelle j'ai gardé ce redimensionnement pour les pièces LEGO à imprimer.




##### 3. Impression des plaques LEGO

J'ai ensuite réalisé un petit kit de différentes plaques LEGO afin de me donner un maximum de liberté dans la conception de mon mécanisme.

* Voilà la plaque du slicer de la prusa avant impression:

![](../images/lego-plate.JPG)

* Voici les pièces imprimées:

![](../images/lego-plate-printed.jpeg)

##### 4. FlexKit au complet

* Voici le kit au complet, toutes pièces réunies:

![](../images/complete-flexkit.jpeg)

##### 5. Flexture Mecanism - "Trigger"

Voici le **résultat final de mon montage flexlink**  qui est une gachette simplifiée : la pression sur l'une des partie déclenche le mécanisme, tandis que la traction sur l'autre partie permet de ré-amorcer le mécanisme.

* **En photos:**


![](../images/meca-charge.jpeg) **CHARGE**
![](../images/meca-detendu.jpeg) **DETENTE**

* **En video: **

<iframe width="682" height="384" src="https://www.youtube.com/embed/tjimsYXekBE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>



