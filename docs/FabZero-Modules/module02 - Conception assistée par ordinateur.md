# 2. Conception Assistée par Ordinateur

Dans cette unité, nous avons appris les rudiments de la conception 3D par ordinateur. Par la maitrise des outils de conception numérique, il est possible de designer des objets et de les modifier à sa guise avant même leur fabrication physique. Cela permet aussi de créer des pièces uniques et totalement personnalisées très précisément. Il est aussi possible de partir de modèles existants afin de les modifier partiellement en adéquation avec le projet réalisé. 

## Mécanisme compliants
Des applications intéressantes réalisables par conception numérique sont les [mécanismes compliants](https://en.wikipedia.org/wiki/Compliant_mechanism) ou mécanismes de flexion. Ces mécanismes ne possèdent **pas de pièces mobiles** (engrenages, roues,...) mais permettent quand même de réaliser des mouvements. Les avantages de ces mécanismes sont nombreux:
* Usure limitée des pièces
* Stabilité dans le temps
* Facilité de fabrication, souvent imprimables en une seule pièce 3D
* Nombre important d'applications possibles (interrupteur, ressort, pièces à mouvement circulaire,...)

### Flexlinks
Les exemples de mécanismes de flexion que nous avons approfondis au fablab sont les flexlinks, des mécanismes dimensionnés pour pouvoir se fixer à diverses pièces de Lego afin de former un mécanisme fonctionnel tel que le système bistable ci-dessous.
![](https://cdn.thingiverse.com/assets/2a/08/d7/35/8f/Animation.gif)

## But de la lacon 
Concevoir un Flexlink à l'aide d'un logiciel de conception 3D.

## Realistion du Flexlink 
J'ai utilisé [OpenScad](https://openscad.org/), un logiciel de conception open source, paramétrique et vectoriel permettant de réaliser des modèles 3D. Le mot paramétrique signifie que dans ce logiciel, toutes les formes sont encodées sous forme d'équations, permettant de modifier partiellement l'objet en quelques lignes de code. Le logiciel est vectoriel, ce qui signifie qu'une forme peut être agrandie ou réduite de manière infinie sans perte d'information. 
### Prise en main d'OpenScad
OpenScad s'utilise en créant un code définissant une combinaison d'objets géométriques disposés dans un espace commun. Les objets peuvent être disposés à une position précise de l'espace et peuvent intéragir de différentes manières entre eux selon les opérateurs utilisés.

#### Exemples de codes OpenScad basiques
Un grand nombre de fonctions peuvent être trouvées sur la [CheatSheet d'OpenScad](https://openscad.org/cheatsheet/), ou sur le logiciel en lui même, qui intègre une écriture prédictive du contenu des fonctions (comme sur l'image ci-dessous).

![](../images/openscad-predict.png)

**Quelques codes de formation d'objet**

```

cube(10, center=true);                     // creation d'un cube de 10 de côté

cylinder(10, 3);                           // creation d'un cylindre de hauteur 10 et de rayon 3


cube([10, 5, 20], center=true);            // changement de dimensions d'un cube


cube([size, size/2, size*2], center=true); // utilisation des variables dans les dimensions d'un cube
```

**Quelques codes de déplacement d'objets et autres**

```
translate([x=1,y=2,z=3])cube(10)           // translation d'un cube de 10 de coté de 1 en x, 2 en y et 3 en z

rotate([45,45,0])cube(10)                  // rotation d'un cube de 45 degrés sur les axes X et Y et 0 degrés sur l'axe Z

fs=0.01;                                    // longueur maximale des segments

fn=150;                                     //nombre de segments pour composer un cercle
```

 **Quelques codes opérateurs (combinaison d'objets)**
```

union(){                    // somme des deux volumes --> utile pour eviter les bugs lors de l'impression
    cube()
    sphere()
}

difference(){               // difference "from cube to sphere" = extrusion
    cube()
    sphere()
}

hull(){                     //Va mettre un "film plastique" autour des points saillants des formes choisies
    cube()
    sphere()
}


```
### Conception dans OpenScad de mon flexlink
J'ai choisi de recréer le Flexlink à 2 têtes reliées par une simple barre comme dans la vidéo plus haut. 
* J'ai d'abord [téléchargé](https://cdn.thingiverse.com/assets/ff/57/7f/c8/23/1_FF.STL) le fichier STL trouvé sur le site flexlinks, et l'ai ouvert sur FreeCad afin de mesurer les proportions approximatives de chaque partie de la pièce. J'ai réalisé cela grâce à l'outil de mesure de FreeCad, qui permet d'indiquer la distance entre une sélection de 2 points
.
![](../images/flex-mesure.jpeg)

* J'ai basé la dimension du trou (r_hole) sur celle des briques de Lego, de diamètre = 4.8 et donc de rayon = 2.4

![](../images/lego.png)

* Toutes les dimensions des paramètres ont été déterminées au début du code individuellement de manière paramétrique, en fonction de r_hole. Grâce à cela, un ajustement sur r_hole est la seule chose à effectuer s'il faut adapter la taille de la pièce lors de l'impression. (P ar exemple, si l'on constate qu'il faut définir un r_hole=2." afin d'imprimer une pièce avec un trou de 2.4 de rayon, il suffira de remplacer le ```r_hole = 2.4 ``` par ```r_hole = 2.3 ``` pour remettre la totalité  de la pièce à bonne échelle et imprimer un objet de lq taille attendue)

* **Voici mon code commenté pour créer le FlexLink**
nb: fichier .scad téléchargeable [ici](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Code%20OpenScad/exo-FLEX.scad/)

```
/*
FILE: exo-FLEX.scad
AUTHOR: Lionel Compere Leroy
DATE: 25/02/2022
LICENCE : Creative Commons Attribution 4.0 International (CC BY 4.0)
*/
//LE CODE EST ECRIT DE MANIERE PARAMETRIQUE, CHAQUE VARIABLE EST DEFINIE AU DEBUT ET DEPEND DE LA VALEUR DU RAYON DU TROU, CE QUI PERMET D'AJUSTER CETTE VALEUR A LA TAILLE D'IMPRESSION REELLE QUE L'ON OBTIENDRA 
//                                              ---> SEULE LA MODIFICATION DU RAYON (r_hole) EST NECESSAIRE A UN AJUSTEMENT 


// ______________________choix de la definition et du rendu (150 segments de polygone pour former un cylindre)_________________________________
$fs=0.01;
$fn=150;

//________________________Definition du rayon du trou en fonction du rayon d'un cylindre Lego (diametre de 4,8 mm)_____________________________
r_hole=2.4;

//________________________dimensions ci-dessous estimées sur base du modèle de FFlexLink que je voulais reproduire________________________
L_tot=r_hole*22.46;         //longueur totale du FlexLink (de l'origine à l'une des extrémités)
H_tot=r_hole*2.54;          //hauteur de la pièce (la meme partout)
l_tot=r_hole*2.96;          //largeur de la piece 
l_stick=r_hole*0.4;         // largeur de la barre de liaison
sp_holes=r_hole*(4/3);      // espace entre les trous respectant les proportions des briques LEGO (3.2 mm d'espace, et donc 4/3 x le rayon des trous (2.4 mm)

L_stick=L_tot-(4*((2*r_hole)+(l_tot/2-r_hole))+2*sp_holes);         //longueur de la barre de liaison (de l'origine à l'une des têtes) à partir de la longueur totale du FlexLink à laquelle je retire 1) les 4 diametres des trous, 2)les 2 espaces entre-trous, 3)les 4 bordures separant le bord du trou de l'extrémité de la pièce          
                                                                    -->permet que la barre centrale s'arrete net au bord des trous


//_________________________CREATION D'UNE DES DEUX MOITIE DU FLEXLINK_________________________________

module un_round(){          //Creation d'un module non-arrondi qui presentera une discontinuité concave au niveau de la connexion de la tête du module à la barre centrale
difference(){
    hull(){     //2 Cylindres enrobés par la fonction hull forment une tete de forme "stade d'athlétisme" (2 demi cercles relies par des sections droites) 
        translate([(L_tot/2)-(l_tot/2),0,0])cylinder(h = H_tot, r = l_tot/2);
        translate([(L_stick/2)+(l_tot/2),0,0])cylinder(h = H_tot, r = l_tot/2);}

    translate([(L_tot/2)-(l_tot/2),0,-0.1])cylinder(h = H_tot+1, r = r_hole);
    translate([(L_stick/2)+(l_tot/2),0,-0.1])cylinder(h = H_tot+1, r = r_hole);//On soustrait à cette forme de "stade" les 2 cyclindres, dont les translations sont réfléchies afin d'obtenir 
}

translate([0,-l_stick/2,0])cube([L_stick/2, l_stick, H_tot]);//creation de la barre de liaison 
translate([L_stick/2.6,-(l_stick*3),0])cube([L_stick/6.9, l_stick*6, H_tot]); //Creation d'un bloc parralellepipedique de liaison entre la barre centrale et la tete, afin de pouvoir le vider ensuite pour obtenir une liaison "smooth" en une seule courbe entre barre et tete

}
r_round=r_hole*1.85;
module round_(){            //Creation d'une liaison plus "smooth" entre tete et barre par extrusion du parralelepipede formé à l'étape précédente à l'aide d'un cylindre de rayon plus élevé
    //NB: le "r_round" a été choisi par essai-erreur, en trouvant la valeur la plus adequate à former la courbe continue et "smooth" attendue
difference (){
    un_round();
    
    translate([L_stick/2.6,r_round+l_stick/2,-0.1])cylinder(h = H_tot+0.2, r = r_round);
    translate([L_stick/2.6,-(r_round+l_stick/2),-0.1])cylinder(h = H_tot+0.2, r = r_round);// soustraction des cylindre et formation des 2 courbes (la meme de chaque coté de la tête)
}
}
module sym_round(){         //symétrie orthogonale du module arrondi afin d'obtenir un FlexLink à 2 tetes
    
    mirror([1, 0, 0]) round_();
    }

round_();
sym_round();

module bigger(){            //creation d'un module de meme type, mais plus long par translation des tetes et insertion d'une barre centrale plus longue 
    translate([10*r_hole,0,0]) round_();
    translate([-10*r_hole,0,0])sym_round();
    translate([0,-l_stick/2,0])cube([L_stick*1.5, l_stick, H_tot]);
    translate([-10*r_hole,-l_stick/2,0])cube([L_stick*1.5, l_stick, H_tot]);
}
translate([0,10*r_hole,0])bigger();
```
* **Voici mon résultat de design de FlexLink** 

![](../images/flex-scad.jpeg)















