/*
Fichier : Cross_Axis.scad

Auteur : Floriane Weyer

Date : 02/03/21

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/
$fn=50;
// Paramètres

// Hauteur de la pièce
height = 6.4;

// Nombre de trous
n = 4;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 0.7;

//Distance selon y entre les deux têtes
head_distance = 40;

// Epaisseur de la tige
beam_thickness = 1.5;

// Calcul de la longueur de la tige et de l'angle
beam_length = sqrt(pow(head_distance,2)+pow((n-1)*distance,2))-(2*radius);
angle = atan(head_distance/((n-1)*distance));


//Définition du module z
module z(){
//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height/2, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height/2, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height/2, r = radius, center = True);
                }
            }
        }
    }
// Création de la première tête
head();

// Création de la deuxième tête
translate([0,head_distance,0]){
    head();
    }    

// Création de la première tige entre les deux têtes   
rotate([0,0,angle])   
translate([radius,-beam_thickness/2,0])
cube([beam_length,beam_thickness,height/2], center = True);
}

z();

translate([0,head_distance,0])
rotate([180,0,0])
z();
