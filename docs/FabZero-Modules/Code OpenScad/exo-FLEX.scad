/*
FILE: exo-FLEX.scad
AUTHOR: Lionel Compere Leroy
DATE: 25/02/2022
LICENCE: Creative Commons Attribution 4.0 International (CC BY 4.0)
*/



// choix de la definition du rendu (150 segments de polygone pour former un cylindre)
$fs=10000000;
$fn=150;

//Definition du rayon du trou en fonction du rayon d'un cylindre Lego (diametre de 4,8 mm)
r_hole=2.4;

//dimensions ci-dessous estimées sur base du modèle de Flexscad que je voulais reproduire
L_tot=r_hole*22.46; //longueur totale du flexscad (de l'origine à l'une des extrémités)
H_tot=r_hole*2.54; //hauteur de la pièce (la meme partout)
l_tot=r_hole*2.96; //largeur de la piece 

//Definition de l'espace entre les trous en fonction de la taille des trous, en respectant les proportions des briques LEGO (3,2 mm d'espace, et donc 4/3 x le rayon des trous (2,4 mmm))
sp_holes=r_hole*(4/3); //

l_stick=r_hole*0.4; // largeur de la barre de liaison

L_stick=L_tot-(4*((2*r_hole)+(l_tot/2-r_hole))+2*sp_holes); //Definition de la longueur de la barre de liaison à partir de la longueur totale du flexscad à laquelle je retire 1) les 4 diametres des trous, 2)les 2 espaces entre-trous, 3)les 4 bordures separant le bord du trou de l'extrémité de la pièce          -->permet que la barre centrale s'arrete net au bord des trous
echo((L_tot/2)-(l_tot/2));
echo((L_stick/2)+(l_tot/2));

//_________________________CREATION D'UNE DES DEUX MOITIE DU FLEXSCAD_________________________________

module un_round(){          //Creation d'un module non-arrondi qui presentera une discontinuité concave au niveau de la connexion de la tête du module à la barre centrale
difference(){
    hull(){     //2 Cylindres enrobés par la fonction hull forment une tete de forme "stade d'athlétisme" (2 demi cercles relies par des sections droites) 
        translate([(L_tot/2)-(l_tot/2),0,0])cylinder(h = H_tot, r = l_tot/2);
        translate([(L_stick/2)+(l_tot/2),0,0])cylinder(h = H_tot, r = l_tot/2);}

    translate([(L_tot/2)-(l_tot/2),0,-0.1])cylinder(h = H_tot+1, r = r_hole);
    translate([(L_stick/2)+(l_tot/2),0,-0.1])cylinder(h = H_tot+1, r = r_hole);//On soustrait à cette forme de "stade" les 2 cyclindres, dont les translations sont réfléchies afin d'obtenir 
}

translate([0,-l_stick/2,0])cube([L_stick/2, l_stick, H_tot]);//creation de la barre de liaison 
translate([L_stick/2.6,-(l_stick*3),0])cube([L_stick/6.9, l_stick*6, H_tot]); //Creation d'un bloc parralellepipedique de liaison entre la barre centrale et la tete, afin de pouvoir le vider ensuite pour obtenir une liaison "smooth" en une seule courbe entre barre et tete
}
r_round=r_hole*1.85;
module round_(){            //Creation d'une liaison plus "smooth" entre tete et barre par extrusion du parralelepipede formé à l'étape précédente à l'aide d'un cylindre de rayon plus élevé
    //NB: le "r_round" a été choisi par essai-erreur, en trouvant la valeur la plus adequate à former la courbe continue et "smooth" attendue
difference (){
    un_round();
    
    translate([L_stick/2.6,r_round+l_stick/2,-0.1])cylinder(h = H_tot+0.2, r = r_round);
    translate([L_stick/2.6,-(r_round+l_stick/2),-0.1])cylinder(h = H_tot+0.2, r = r_round);// soustraction des cylindre et formation des 2 courbes (la meme de chaque coté de la tête)
}
}
module sym_round(){         //symétrie orthogonale du module arrondi afin d'obtenir un flexscad à 2 tetes
    
    mirror([1, 0, 0]) round_();
    
    }

round_();
sym_round();

module bigger(){            //creation d'un module de meme type, mais plus long par translation des tetes et insertion d'une barre centrale plus longue 
    translate([10*r_hole,0,0]) round_();
    translate([-10*r_hole,0,0])sym_round();
    translate([0,-l_stick/2,0])cube([L_stick*1.5, l_stick, H_tot]);
    translate([-10*r_hole,-l_stick/2,0])cube([L_stick*1.5, l_stick, H_tot]);
}
translate([0,10*r_hole,0])bigger();