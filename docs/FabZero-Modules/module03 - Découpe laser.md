# 4. Découpe assistée par ordinateur
## Principe de la découpe laser
La découpe laser consiste à utiliser un faisceau laser dirigé par 2 axes afin de graver des sillons dans un matériau. Les sillons peuvent être plus ou moins profonds, et donc constituer:
* un dessin (passage superficiel du laser, laissant juste une trace de combustion)
* un pli (passage du laser suffisant à extruder une partie du matériau, mais sans passer à travers, permettant de plier la section)
* une découpe (passage du laser profond, à travers le matériaux, séparant les 2 cotés du segment)

### Types de fichier
Les fichiers utilisés par la découpeuse laser peuvent être de 2 grands types:
* **Vecteurs**

Dans ce cas, le fichier est une combinaison d'équation, dont le faisceau laser suivra la trajectoire pour effectuer la découpe.
* **Rasters**

Dans ce cas, le fichier est un classique ensemble de pixel, et la découpeuse se comportera à la manière d'une imprimante à encre, en appliquant le faisceau 

### Applications de la découpe laser
Une découpeuse laser permet donc de combiner différemment ces types de découpe afin de créer des objets dont certaines parties peuvent être dessinés, découpés, ou pliés. C'est pourquoi on peut par exemple l'utiliser pour constituer des kirigamis, qui demandent une découpe pour leur contour, et une gravure plus légère pour constituer leur plis. 

Voici un exemple parlant de ce qu'est un kirigami: 

![](https://scholar.harvard.edu/files/choi/files/square_to_circle_experiment.gif?m=1566285743)

## Logiciels de dessin 2D
Afin de créer des fichiers vectoriels exploitables par une découpeuse, j'ai utilisé le logiciel de dessin [Inkscape](https://inkscape.org/fr/). Ce logiciel permet de créer diverses formes, et de colorer les vecteurs dans des teintes différentes auxquelles on pourra attribuer à l'aide de la machine une puissance et une vitesse de découpe propres, et donc des profondeurs de découpe différentes. Le format de sortie à utiliser est le svg.
Voilà à quoi ressemble l'interface d'inkscape:

![](../images/inkscape.JPG/)


## Machine utilisée
Cette section se concentrera sur la machine que j'ai eu l'occasion de prendre en main lors des travaux pratiques, mais voici un [lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md) vers les information concernant toutes les autres découpeuses laser du FabLab de l'ULB.
### Consignes de sécurité
Toutes les machines de découpe laser sont des instruments dangereux car ils produisent un laser de haute température et luminosité, dont le but est de carboniser le matériau ciblé. Toute manipulation hasardeuse peut donc mener à des accidents graves tels que des incendies, des lésions occulaires ou encore des intoxications dues aux fumées nocives. C'est pourquoi il est important de prendre les précautions suivantes avant de passer à la découpe:

* Activer l’air comprimé, qui éteindra directement les flammes induites par le laser
* Allumer l’extracteur de fumée afin d'éliminer les fumées nocives
* Localiser le bouton d’arrêt d’urgence
* Localiser l'extincteur au CO2 et la couverture anti-feu
* Surveiller la machine jusqu'à la fin de la découpe
* Utiliser un matériau adéquat (carton, contreplaqué, textile, acrylique) --> liste totale des matériaux [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)


### Présentation de la machine
La machine utilisée par notre groupe au FabLab est la [MuseCore Full Spectrum Laser](http://laser101.fslaser.com/materialtest), une découpeuse compacte et intuitive d'utilisation, que l'on peut contrôller à distance via connexion wifi sur son PC personnel.
![](/docs/images/machine-muse-core.jpg/)
### Connexion à la machine
La machine de découpe MuseCore fonctionne en WiFi, ce qui permet d'utiliser son interface directement sans fil via n'importe quel navigateur PC. Pour cela, il faut:
* 1.Se connecter au wifi local du nom de "lasercutter"
* 2.Encoder le mot de passe "FABLABULB2019"
* Aller via navigateur web à l'adresse suivante https://fs.local

### Calibration de la machine
Afin de calibrer la machine pour que le point focal du faisceau laser soit concentré au niveau du matériau, il suffit de glisser en dessous de la tête laser un étalon permettant de régler la hauteur de la tête par rapport à l'épaisseur de la plaque à découper.

### Périmètre de découpe
Afin de s'assurer que l'image à découper ne dépasse pas du matériau placé dans la machine, la première chose à faire est de lancer le périmètre via la fonction de la découpeuse _"Run perimeter"_ comme visible entouré en rouge ci-dessous dans l'interface de la MuseCore. Lors de la réalisation du périmètre, il peut être nécéssaire de déplacer la matériau afin de s'assurer que le périmètre soit totalement inclus dans la surface disponible.

![](../images/retina-perimeter.JPG/)



## Realisation d'un kirigami

J'ai décidé de créer un kirigami de forme pyramidale à base octogonale.

### Conception d'étalons tests
Avant de réaliser le kirigami, nous avons réalisé des étalonnages afin d'observer le résultat de différentes combinaisons de puissance et vitesses sur le matériau qui sera utilisé pour nos kirigamis. Pour cela, nous avons crée 2 étalons, comportant à chaque fois plusieurs couleur différente attribuables à des paramètres de découpe: 
* **Premier étalon** , combinant des vitesses de 20, 50 et 90 % et des puissances de 20, 50 et 90 % (9 combinaisons). Cet étalon permet d'avoir une vision globale des combinaisons possibles et est réalisé directement sur l'interface graphique de la machine, avec un choix direct des paramètres liés aux couleurs.

![](../images/etalon-hexa.png/)

* **Deuxième étalon** , combinant des vitesses de 10 à 100 % pour 2 puissances 80 et 90 % (9 combinaisons). Celui ci permet de tester plus précisément la pliure des différentes combinaison. Il a été crée sur inkscape, et a ensuite été importé au format .svg sur l'interface de la machine, ou chaque couleur a été attribuée à une combinaison vitesse-puissance. 

![](../images/etalon-latte.JPG)

Télécharger l'étalon de pliure ci-dessus [ici](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Mod%C3%A8les%20inkscape/PLI.svg/)


### Etalonnage de la machine
Nous avons donc lancé chacun de ces deux fichiers, avec comme résultat que la combinaison optimale était:
* vitesse=70% et puissance=90% pour les plis
* vitesse=50% et puissance=90% pour la découpe

![](../images/true-etalon-hexa.jpeg)
![](../images/true-etalon-latte.jpeg)

### Conception du kirigami sur inkscape
Voici la réalisation de mon kirigami pyramidal, dont le lien se trouve [ici](https://gitlab.com/-/ide/project/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/tree/main/-/docs/FabZero-Modules/Mod%C3%A8les%20inkscape/kirigami.svg/). Il consiste en une combinaison d'un octogone et de 8 triangles.
![](../images/modele-kirigami.JPG/)

### Découpe du kirigami
La découpe du kirigami a été faite en choisissant comme paramètres vitesse=70% + puissance=90% pour les plis (traits de liaison entre l'octogone et les triangles) et vitesse=50% + puissance=90% pour la découpe (cotés externes des triangles)


![](../images/flat-kirigami.jpeg) kirigami en sortie de découpe

![](../images/open-kirigami.jpeg) Kirigami détaché de sa plaque

![](../images/finish-kirigami.jpeg) Kirigami une fois replié

Comme on peut le voir sur le résultat, c'est plus ou moins réussi, avec comme défaut le fait que les paramètres choisis pour la pliure semblaient un peu trop puissants et causaient le détachement partiel de certains triangles. Cela est peut être du au fait que le matériau utilisé n'était pas exactement le même pour les étalonnages et le kirigami.



