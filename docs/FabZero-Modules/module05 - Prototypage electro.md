# 5. Electronique 1 - Prototypage

Le but de cette unité est de prendre en main la programmation et le prototypage de cartes arduino. Deux éléments sont cruciaux à la mise en place de dispositifs expérimentaux à l'aide d'un arduino: l' **acquisition de données** issues de capteurs et l'utilisation de ces données pour **moduler l'input d'un module tiers**. J'ai donc acquis une carte de type arduino [(elegoo)](https://www.elegoo.com/collections/uno-r3-starter-kits/products/elegoo-uno-most-complete-starter-kit) avec pour objectif final d'apprendre à l'utiliser à travers un projet de dispositif réagissant par un son de fréquence différente en fonction de la distance enregistrée par le capteur de distance à ultrasons. 

##  Prise en main de l'arduino

J'ai téléchargé le logiciel [arduino](https://www.arduino.cc/en/software) afin de pouvoir programmer ma carte depuis mon ordinateur. Afin d'avoir les informations nécéssaires, j'ai téléchargé les [exemples de code et guides de prise en main](http://69.195.111.207/tutorial-download/?t=UNO_R3_Project_The_Most_Complete_Starter_Kit_V2.0) fournis avec la carte elegoo.
### 1.Connexion de l'arduino à l'ordinateur:
Afin de pouvoir lier la carte programmabe au logiciel arduino, il faut choisir le bon modele de carte ainsi que le port d'entrée à exploiter. 

![](../images/carte-arduino.png ) ![](../images/port-arduino.png )

J'ai donc choisi comme carte **"Arduino Uno"** et comme port de série **"COM7 Arduino Uno"** 
### 2.Premiers essais de codage et montage:
Après connexion de l'Arduino par un cable usb à l'ordinateur, il est possible d'interragir avec via le logiciel arduino. Les commandes permettent entre autres d'envoyer des signaux à travers les broches de connexion, afin de les utiliser comme sortie, ou d'en réceptionner les signaux, afin de les utiliser comme entrées. 
#### Essai d'interraction avec des LEDs
L'une des manières les plus simple pour entamer l'apprentissage Arduino consiste à interragir avec des diodes led.
J'ai monté un circuit arduino sur une breadboard composé de 2 LEDs, chacun connectés à un pin différent de l'Arduino et au "_Ground_".

![](../images/led.jpeg)

 J'ai ensuite réalisé un code permettant de faire clignoter ces 2 LEDs en alternance.

Pour cela, dans la partie "_void setup()_", qui se lance automatiquement **une seule fois** dès que l'Arduino est sous tension, il faut: 
* Définir les broches utilisées et leur assigner une valeur.
* Définir l'utilisation de chaque broche (ici sortie, donc _OUTPUT_ )
```
void setup() {
  #define RED 3
  #define BLUE 4
  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
}
```
Ensuite, dans la partie "_void loop()_", qui se répète à l'infini après execution du _void setup_ , il faut alternativement allumer chacun des 2 LEDs. 
* La fonction digitalWrite permet d'assigner un état (HAUT / BAS) à une broche (et donc d'allumer/éteindre les LEDs)
* La fonction delay permet d'induire un temps de latence entre l'execution des 2 duos de fonctions: 

```

void loop() {
  digitalWrite(RED , HIGH);
  digitalWrite(BLUE , LOW);
  delay(50);
  digitalWrite(RED , LOW);
  digitalWrite(BLUE , HIGH);
  delay(50);
                      
}
```

## Projet du "Music - Meter"
Pour apprendre à récupérer et à exploiter les inputs fournis par un capteur, j'ai décidé de me baser sur 2 codes fournis dans les exemples de la carte Elegoo. L'un des codes permettait d'afficher sur le moniteur de série une mesure de distance effectuée par le capteur à ultrasons, tandis que l'autre permettait d'émettre différentes notes via un buzzer. Mon idée était donc de récupérer l'output du capteur de distance (ultrasons) et de l'utiliser comme input du buzzer dans le but d'émettre une note différente correspondante à chaque intervalle de distance mesuré. 


J'ai donc branché le capteur sur 2 pins qui serviront d'entrées (pins 11 et 12) et le buzer sur un pin qui servira de sortie (pin 8), comme on peut le voir sur les photos de mon montage.

![](../images/arduino-connexion.jpg)![](../images/montage-small.jpg)


Les 2 modules (capteur ultrassons et buzzer) nécéssittent chacun l'inclusion d'une bibliothèque attitrée intégrant des fonctions appropriées. J'ai obtenu les bibliothèques via les fichiers fournis dans le kit de démarrage elegoo.

![](../images/biblio.png)

Ces bibliothèques s'intègrent sous forme de fichier .zip. Ces bibliothèques peuvent ensuite être incluses par la fonction suivante:

```
#include "SR04.h"
#include "pitches.h"

```
Ensuite on peut définir les broche dans lesquelles l'émetteur (TRIG) et le récepteur (ECHO) infrarouges seront branchés (les 2 autres cables du module infrarouge servant à l'alimentation et à la masse).


```
#define TRIG_PIN 12
#define ECHO_PIN 11 

```
On fait ensuite appel à la fonction sr04 pour lui indiquer les broches assignées à la mesure des signaux, cette fonction permet d'obtenir un output de distance qu'on appellera "a".
```
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);
long a;
```
On définit ensuite une liste de notes que l'on pourra exploiter pour faire sonner le buzzer.

```
int melody[] = {
  NOTE_C5, NOTE_D5, NOTE_E5, NOTE_F5, NOTE_G5, NOTE_A5, NOTE_B5, NOTE_C6};
int duration = 50;  // 500 miliseconds
 
```
On démarre la récupération de "a" par le port de série ()
```

void setup() {
   Serial.begin(9600);
   delay(100);

}
```
Dans la void loop, j'ai assigné pour chaque intervalle de distance (valeur de "a") l'émission d'une des notes de la liste "melody". La boucle tournant de manière infinie permet donc l'émission d'une note différente dès que l'intervalle de distance dans lequel se situe a change. Le delay de 10 permet de mettre 100 millisecondes entre chaque actualisation de la valeur de a.
```
void loop() {
  Serial.println (a);
   a=sr04.Distance();
   if (a < 5)
{
  tone(8, melody[8], duration);
}
   if (a > 5 and a < 10)
{
  tone(8, melody[7], duration);
}
   if (a > 10 and a < 15)
{
  tone(8, melody[6], duration);
}
   if (a > 15 and a < 20)
{
  tone(8, melody[5], duration);
}
   if (a > 20 and a < 25)
{
  tone(8, melody[4], duration);
}
   if (a > 25 and a < 30)
{
  tone(8, melody[3], duration);
}
   if (a > 30 and a < 35)
{
    tone (8, melody[2], duration);
}
   if (a > 35 and a < 40)
{
  tone(8, melody[1], duration);
}
    
   
   delay(10);
}
```

Voici le **résultat final de mon projet** : [VIDEO DU MUSIC - METER](https://youtu.be/nyJ_kJ0ed2g)
<iframe width="682" height="384" src="https://www.youtube.com/embed/nyJ_kJ0ed2g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>






