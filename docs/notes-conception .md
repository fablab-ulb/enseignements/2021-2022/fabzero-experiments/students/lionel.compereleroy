# 2. Conception Assistée par Ordinateur

## compliant mecanism

Mecanismes n'ayant pas besoin de pieces mobiles
--> se basent sur la geometrie 
### flexture mecanism
Pieces fixes pouvant se deformer
### bistable mecanism
 * Objet avec 2 positions de stabilite

### auxetics

mecanisme qui va changer les tailles dans plusieurs dimensions en ne le deformant que dans une dimension

## Conception formats
### vecteurs
Texte code qui contient les indications (equations) à suivre pour former un dessin

## Logiciel de design 3d

### "Equivalent du pixel"
#### sketchup
trop simple, on est bloque au bout d'un moment
#### blender
le "photoshop de la 3D ", tres puissant 

permet aussi de creer des animations physiques des objets 

### "Geometry Design" (vectoriel)
#### OpenSCAD
marche en decrivant sa piece en termes geometriques
puissant car permet de 
Dimensions de base = mm

* **Exemple de codes basiques**
```
#creation d'un cube
cube(10, center=true);
#changer les dimensions d'un cube
cube([10, 5, 20], center=true);
#utiliser des variables dans les dimensions
cube([size, size/2, size*2], center=true);
#Changer render
fs=500

```
* **Exemple de codes operateurs**
```
#somme des deux volumes --> utile pour eviter les bugs lors de l'impression
union(){
    cube()
    sphere()
}

#difference "from cube to sphere" = extrusion
difference(){
    cube
    sphere
}

#Va "mettre un fil plastique autour des points saillants"
hull(){
    cube()
    sphere()
}

#rotation
rotate([45,45,0])cube(10)

#en fonction du tps avec $t
```
cf: [Openscad cheatsheet](https://openscad.org/cheatsheet/)

/!\ on peut croire que la forme n'est pas vectorielle, mais en fait elle l'est, et c'est le "visual render" qui n'est pas precis 
* **Aide d'une librairie**
[BOSL](https://github.com/revarbat/BOSL) par exemple
* **Fichier de [Thingiverse](https://www.thingiverse.com/)**
#### Antimony

constitue de "boites de code" reliees ensemble, dans lesquelles on peut modifier des parametres, ce qui permet de 


