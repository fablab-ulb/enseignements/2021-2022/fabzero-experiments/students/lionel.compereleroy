# PRESENTATION

Vous voici sur mon blog, réalisé dans le cadre du cours **"How To Make (almost) Any Experiments"**, alias **"FabZero Experiments"**. Ce cours a pour but d'introduire les étudiants à la manipulation des outils de fabrication numérique présents au sein d'un FabLab. Le cours se compose d'une phase d'apprentissage et de formation à ces outils, suivie d'une phase de mise en pratique de ces apprentissages à travers d'un projet de notre choix, basé sur l'un des [17 objetifs du développement durable de l'ONU](https://www.undp.org/fr/sustainable-development-goals)

Ce blog aura pour but de servir de _"carnet de bord"_, afin de garder une trace des apprentissages réalisés lors de ce cours.
Nous parcourrons entre autres ensemble:

* Le processus de mise en place et de personnalisation d'un site internet personnel via GitLab
* La prise en main et le fonctionnement...
    * Des logiciels de fabrication numérique
    * Des machines de fabrication numérique
* Le déroulement des scéances d'exercice
* Les différentes phases de la réalisation du projet
    * Choix de la problématique à résoudre
    * Solutions imaginées, idées émises
    * Réalisabilité --> Choix de la solution à mettre en pratique
    * Conception par logiciels
    * Mise en place physique du projet
* ...



## Que dire sur moi ?

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/-/raw/b468c1d74cdc1210df69dac33ce247e5ab351720/docs/images/Moi.jpg)

Je m'appelle Lionel Compere Leroy, et suis un étudiant de Master 2 en Sciences Agronomiques. J'ai 23 ans, je vis à Bruxelles dans un chouette appartement au dessus de chez ma grand-mère, et j'adore cuisiner. J'ai 2 soeurs (une grande et une petite), un lapin, un chat et des guppys. 

(La photo, c'est moi au sommet de l'église du sacré coeur de Montmartre, beaucoup de marches pour arriver là haut je vous l'assure !)

## Mon histoire

Je suis né à Forest (Bruxelles), loin des hôpitaux, dans le lit de mes parents le 8 décembre 1998, par une matinée enneigée. J'ai vécu une enfance heureuse, entre ma scolarité au sein d'une petite école dans un verger, le patro (un mouvement de jeunesse), mes amis que je voyais souvent après l'école et mes grands parents, avec qui je passais des après-midi entiers chaque semaine. 

J'ai commencé la musique vers l'âge de 6 ans, en faisant du solfège à l'académie de Forest. J'ai pu y apprendre la guitare, instrument que je n'ai jamais laché depuis. Je passais beaucoup de temps avec mes amis du quartier: nous étions souvent en balade dans les bois à vélo, curieux de découvrir chaque sentier, d'attraper des têtards, ou alors juste à la maison en train de jouer aux playmobils ou aux legos. Mes parents nous emmenaient chaque hiver en gite avec leurs amis pour le nouvel an, et ces moments m'ont donné l'amour des jeux de socciété et des longues ballades dans la nature. 

Mon adolescence a été riche d'expériences : J'ai décidé de devenir animateur au patro à 16 ans, et ait organisé un nombre incalculable de journées, week-end et camps mémorables avec les amis de mon staff. J'ai aussi eu l'occasion de voyager, au Burkina Faso dans le cadre d'un projet de coopération au développement avec l'ONG DBA ; en Californie pour rendre visite à mon oncle à San-Diego; ou en Inde pour aller voir ma soeur qui faisait sa 2 ème rhéto là-bas. 

C'est aussi lors de mes secondaires que j'ai développé un intérêt grandissant pour la science. En effet, j'avais toujours été fasciné par la nature et les expériences de chimie que mon père, prof de sciences, me montrait, mais c'est à ce moment que j'ai décidé d'en pprofondir l'apprentissage. J'ai donc choisi de prendre une option math-sciences pour me perfectionner et en découvrir toujours plus de choses passionnantes.  

A la fin de mon parcours scolaire secondaire, je savais que je voulais étudier les sciences à l'université, mais aussi pouvoir travailler sur du concret, du fonctionnel. Mon choix s'est alors tourné vers la bioingéniérie, liant à travers les sciences appliquées la technologie et le vivant.

Mes années d'université ont été géniales, et j'ai été passionné par bon nombre de mes cours. J'ai eu l'occasion de découvrir de nombreux domaines : de la microbiologie à l'informatique, en passant par la botanique ou la brasserie. Je suis également devenu permanent du Foyer Scientifique et Culturel de la plaine (FOSCUP), au sein duquel nous avons organisé de nombreuses soirées musicales ou sorties culturelles.

## Passions

J'ai eu de nombreux loisirs dans ma vie: 

* **L'escalade** , qui m'a fait découvrir la satisfaction du dépassement de soi ainsi que de magnifiques falaises belges.
* **La gymnastique** , qui m'a montré l'importance de la persévérance et de la patience.
* **La guitare** , qui m'a donné envie de créer et faire passer des émotions par la musique.
* **Le skateboard** , qui m'a montré à quel point c'est important d'y croire et de ne pas se décourager (et qui m'a permis de ne pas rester à me tourner les pouces devant mon écran pendant les confinements)
* **La nature** , qui n'est peut être pas un loisir en tant que tel, mais une source d'émerveillement perpétuel, qui permet de se ressourcer 

## Expérience professionnelle ![](https://www.permafungi.be/wp-content/uploads/2018/10/logo-png-80x80.png)
J'ai pu faire ​mes premiers pas dans le monde du travail cet été, lors de mon stage de longue durée à  [Permafungi](https://www.permafungi.be/), une entreprise de culture de champignons. Cette expérience m'a fait découvrir le monde fascinant de l'agriculture urbaine, et m'a ouvert aux problématiques du traitement des déchêts et de l'approvisionnement local en nourriture. Cela m'a aussi donné un bel exemple de ce qu'est l'économie circulaire, et de la puissance des _low-tech_ dans la construction d'un avenir plus durable. Outre la production de champignon, j'ai aussi pu m'essayer là-bas à la production de myco-matériaux, substitut du plastique concu à partir de mycélium et de déchêts organiques produits par la culture du pleurote.


